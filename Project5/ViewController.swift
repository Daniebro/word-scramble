//
//  ViewController.swift
//  Project5
//
//  Created by Danni Brito on 11/8/19.
//  Copyright © 2019 Danni Brito. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var allWords = [String]()
    var usedWords = [String]()
    var currentWord = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(startGame))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promptForAnswer))
        
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt"){
            if let startWords = try? String(contentsOf: startWordsURL){
                allWords = startWords.components(separatedBy: "\n")
            }
        }
        
        if allWords.isEmpty {
            allWords = ["silkworm"]
        }
        
        let defaults = UserDefaults.standard
        currentWord = defaults.string(forKey: "currentWord")!
           
        if let savedData = defaults.object(forKey: "usedWords") as? Data {
            let jsonDecoder = JSONDecoder()
            do{
                usedWords = try jsonDecoder.decode([String].self, from: savedData)
            } catch {
                print("failed to load people")
            }
        }
        
        title = currentWord
        
        
        
    }
    
    @objc func startGame(){
        currentWord = allWords.randomElement()!
        title = currentWord
        usedWords.removeAll(keepingCapacity: true)
        save()
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        return cell
    }
    
    @objc func promptForAnswer(){
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField()
        
        let submitAction = UIAlertAction(title: "Submit", style: .default){
            [weak self, weak ac] _ in
            guard let answer = ac?.textFields?[0].text else { return }
            self?.submit(answer)
        }
        
        ac.addAction(submitAction)
        present(ac, animated: true)
    }
    
    func submit(_ answer:String) {
        let lowerAnswer = answer.lowercased()
        
        let errorTitle: String
        let errorMessage: String
        
        if isPossible(word: lowerAnswer) {
            if isOriginal(word: lowerAnswer){
                if isReal(word: lowerAnswer){
                    usedWords.insert(answer.lowercased(), at: 0)
                    let indexPath = IndexPath(row: 0, section: 0)
                    tableView.insertRows(at: [indexPath], with: .automatic)
                    save()
                    return
                } else {
                    errorTitle = "Word not Recognized or shorter than 3 letters"
                    errorMessage = "You can't just make them up, ya know!"
                    
                }
            } else {
                errorTitle = "Word already used"
                errorMessage = "Be more original!"
            }
        } else {
            guard let title = title else {return}
            errorTitle = "Word not possible"
            errorMessage = "You can't speel that word from \(title.lowercased())"
        }
        showErrorMessage(errorTitle: errorTitle, errorMessage: errorMessage)
        
        
    }
    
    func isPossible(word: String)  -> Bool {
        guard var tempWord = title?.lowercased() else { return false }
        
        for letter in word{
            if let position = tempWord.firstIndex(of: letter){
                tempWord.remove(at: position)
            } else {
                return false
            }
        }
        
        return true
    }
    
    func isOriginal(word: String)  -> Bool {
        if word == title {return false}
        return !usedWords.contains(word)
    }
    
    func isReal(word: String)  -> Bool {
        if word.count < 3 {return false}
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        return misspelledRange.location == NSNotFound
        
        
    }
    
    func showErrorMessage(errorTitle: String, errorMessage: String){
        let ac = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac,animated: true)
    }
    
    func save(){
        let jsonEncoder = JSONEncoder()
        let defaults = UserDefaults.standard
        defaults.set(currentWord, forKey: "currentWord")
        if let savedData = try? jsonEncoder.encode(usedWords){
            defaults.set(savedData, forKey: "usedWords")
        } else {
            print("failed to save Data")
        }
    }


}

